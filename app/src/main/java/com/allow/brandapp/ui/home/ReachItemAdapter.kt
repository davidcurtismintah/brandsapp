package com.allow.brandapp.ui.home

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.allow.brandapp.R
import com.allow.brandapp.domain.model.Reach
import com.allow.brandapp.ui.common.ItemAdapter
import com.allow.brandapp.ui.common.bindView
import com.allow.brandapp.ui.home.ReachItemAdapter.ViewHolder
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.LargeValueFormatter


/**
 * creates a [ViewHolder] and binds it to a [Reach].
 */
class ReachItemAdapter(
    val reach: Reach
) : ItemAdapter<ViewHolder>(R.layout.item_brand_reach) {

    /**
     * Creates a [RecyclerView.ViewHolder] and returns it
     * */
    override fun onCreateViewHolder(itemView: View) = ViewHolder(itemView)

    /**
     * Sets up views
     * */
    override fun ViewHolder.onBindViewHolder() {
        with(reach) {
            titleView.text = title
            subTitleView.text = subTitle

            setUpChart(chartView)
        }
    }

    private fun Reach.setUpChart(chartView: BarChart) {
        val entries = companies.sortedByDescending { it.value }.mapIndexed { index, brand ->
            BarEntry(index.toFloat(), brand.value.toFloat())
        }

        val set = BarDataSet(entries, "BrandsInfoSet").apply {
            setColors(intArrayOf(R.color.blue, R.color.orange, R.color.blue), chartView.context)
        }

        val valueFormatter = LargeValueFormatter().apply { setSuffix(arrayOf("", "K", "M", "B", "T")) }
        val chartData = BarData(set).apply {
            barWidth = 0.9f // set custom bar width
            setValueFormatter(valueFormatter)
        }

        chartView.apply {
            data = chartData
            axisRight.isEnabled = false
            axisLeft.setDrawZeroLine(true)
            axisLeft.valueFormatter = valueFormatter
            xAxis.isEnabled = true
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.setDrawGridLines(false)
            xAxis.setDrawLabels(false)
            xAxis.setDrawAxisLine(true)
            legend.isEnabled = false
            description.isEnabled = false
            setFitBars(true) // make the x-axis fit exactly all bars
            invalidate()
        }

    }

    /**
     * Holds views used by [ReachItemAdapter]
     * */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleView by bindView<TextView>(R.id.reach_title)
        val subTitleView by bindView<TextView>(R.id.reach_sub_title)
        val chartView by bindView<BarChart>(R.id.reach_chart)
    }
}