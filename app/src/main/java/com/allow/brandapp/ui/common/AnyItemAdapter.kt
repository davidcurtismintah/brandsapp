package com.allow.brandapp.ui.common

import androidx.recyclerview.widget.RecyclerView

/**
 * [ItemAdapter] for [RecyclerView.ViewHolder].
 */
typealias AnyItemAdapter = ItemAdapter<out RecyclerView.ViewHolder>