package com.allow.brandapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.allow.brandapp.domain.commands.GetBrandsInfoCommand
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.model.BrandsInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BrandsInfoViewModel : ViewModel() {

    val brandsInfoLiveData: LiveData<Outcome<BrandsInfo>> by lazy {
        GetBrandsInfoCommand().execute()
    }
}