package com.allow.brandapp.ui.home

import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.allow.brandapp.R
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.model.BrandsInfo
import com.allow.brandapp.ui.common.snack
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_brands_info.*
import timber.log.Timber

class BrandsInfoActivity : DaggerAppCompatActivity() {

    lateinit var mBrandsInfoViewModel: BrandsInfoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_brands_info)
        recyclerView.layoutManager = LinearLayoutManager(this)

        mBrandsInfoViewModel = ViewModelProviders.of(this).get(BrandsInfoViewModel::class.java)

        mBrandsInfoViewModel = ViewModelProviders.of(this).get(BrandsInfoViewModel::class.java)
        mBrandsInfoViewModel.brandsInfoLiveData.observe(this, Observer {
            it?.let { brandsInfoOutcome ->
                showBrandsInfoOutcome(brandsInfoOutcome)
            }
        })
    }

    private fun showBrandsInfoOutcome(outcome: Outcome<BrandsInfo>) {

        when (outcome) {

            is Outcome.Progress -> {
                Timber.d("Started Obtaining Brands Info")
            }

            is Outcome.Success -> {
                Timber.d("Obtained Brands Info: ${outcome.data}")
                showBrandsInfo(outcome.data)
            }

            is Outcome.Failure -> {
                Timber.e(outcome.error, "Error Obtaining Brands Info")
                snack(recyclerView,
                    getString(R.string.default_error_message),
                    Snackbar.LENGTH_INDEFINITE)
            }
        }
    }

    private fun showBrandsInfo(items: BrandsInfo) {
        val categoryItemAdapters = listOf(
            createAnalysisItemAdapter(items),
            createReachItemAdapter(items)
        )
        recyclerView.adapter = MainListAdapter(categoryItemAdapters)
    }

    private fun createAnalysisItemAdapter(brandsInfo: BrandsInfo) =
        AnalysisItemAdapter(brandsInfo.analysis)

    private fun createReachItemAdapter(brandsInfo: BrandsInfo) =
        ReachItemAdapter(brandsInfo.reach)
}
