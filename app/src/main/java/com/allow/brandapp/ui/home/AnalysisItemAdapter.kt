package com.allow.brandapp.ui.home

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.allow.brandapp.R
import com.allow.brandapp.domain.model.Analysis
import com.allow.brandapp.extensions.toBulleted
import com.allow.brandapp.ui.common.ItemAdapter
import com.allow.brandapp.ui.common.bindView
import com.allow.brandapp.ui.home.AnalysisItemAdapter.ViewHolder

/**
 * creates a [ViewHolder] and binds it to an [Analysis].
 */
class AnalysisItemAdapter(
    val analysis: Analysis
) : ItemAdapter<ViewHolder>(R.layout.item_brand_analysis) {

    /**
     * Creates a [RecyclerView.ViewHolder] and returns it
     * */
    override fun onCreateViewHolder(itemView: View) = ViewHolder(itemView)

    /**
     * Sets up views
     * */
    override fun ViewHolder.onBindViewHolder() {
        with(analysis) {
            titleView.text = title
            subTitleView.text = subTitle
            deltaPhraseView.text = deltaPhrase.toBulleted()
            performancePhraseView.text = performancePhrase.toBulleted()
            summaryPhraseView.text = summaryPhrase.toBulleted()
            winnerPhraseView.text = winnerPhrase.toBulleted()
        }
    }

    /**
     * Holds views used by [AnalysisItemAdapter]
     * */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleView by bindView<TextView>(R.id.analysis_title)
        val subTitleView by bindView<TextView>(R.id.analysis_sub_title)
        val deltaPhraseView by bindView<TextView>(R.id.analysis_delta)
        val performancePhraseView by bindView<TextView>(R.id.analysis_performance)
        val summaryPhraseView by bindView<TextView>(R.id.analysis_summary)
        val winnerPhraseView by bindView<TextView>(R.id.analysis_winner)
    }
}