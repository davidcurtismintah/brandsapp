package com.allow.brandapp

import android.text.TextUtils
import com.allow.brandapp.di.DaggerAppComponent
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber


class BrandsInfoApp : DaggerApplication() {

    companion object {
        private val DEFAULT_REQUEST_TAG = BrandsInfoApp::class.java.simpleName
        lateinit var instance: BrandsInfoApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        Timber.plant(Timber.DebugTree())
    }

    private val requestQueue: RequestQueue by lazy { Volley.newRequestQueue(applicationContext) }

    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) DEFAULT_REQUEST_TAG else tag
        requestQueue.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = DEFAULT_REQUEST_TAG
        requestQueue.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        requestQueue.cancelAll(tag)
    }

    override fun applicationInjector(): AndroidInjector<out BrandsInfoApp> {
        return DaggerAppComponent.builder().create(this)
    }
}
