package com.allow.brandapp.di

import com.allow.brandapp.BrandsInfoApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityBindingModule::class, AppModule::class])
interface AppComponent : AndroidInjector<BrandsInfoApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<BrandsInfoApp>()
}

