package com.allow.brandapp.di

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.allow.brandapp.data.local.BrandsInfoDao
import com.allow.brandapp.data.local.BrandsInfoDatabase
import dagger.Module
import javax.inject.Singleton
import dagger.Provides

@Module
class AppModule {

    /*companion object {
        val MIGRATION_1_2: Migration = object : Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {
                // Change the table name to the correct one
                database.execSQL("ALTER TABLE cryptocurrency RENAME TO cryptocurrencies")
            }
        }
    }*/

    @Provides
    @Singleton
    fun provideBrandsInfoDatabase(app: Application): BrandsInfoDatabase =
        Room.databaseBuilder(app, BrandsInfoDatabase::class.java, "brands_info_db")
            /*.addMigrations(MIGRATION_1_2)*/
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideBrandsInfoDao(database: BrandsInfoDatabase): BrandsInfoDao = database.brandsInfoDao()

}