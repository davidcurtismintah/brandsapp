package com.allow.brandapp.di

import com.allow.brandapp.ui.home.BrandsInfoActivity
import com.allow.brandapp.ui.home.BrandsInfoActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [BrandsInfoActivityModule::class])
    abstract fun bindMainActivity(): BrandsInfoActivity

}