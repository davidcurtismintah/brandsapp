package com.allow.brandapp.extensions

import android.content.res.Resources
import android.text.SpannableString
import android.text.Spanned
import android.text.style.BulletSpan
import android.util.TypedValue
import androidx.annotation.DimenRes
import com.allow.brandapp.R

fun String.toBulleted(): SpannableString {
    val spannable = SpannableString(this)
    spannable.setSpan(BulletSpan(8f.dpToPixels() /*, Color.RED*/), 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    return spannable
}

fun Float.dpToPixels(): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics).toInt()
}

fun getPixels(@DimenRes resId: Int): Int {
    return Resources.getSystem().getDimensionPixelSize(resId)
}