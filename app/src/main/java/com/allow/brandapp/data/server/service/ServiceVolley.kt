package com.allow.brandapp.data.server.service

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.allow.brandapp.BrandsInfoApp
import com.allow.brandapp.data.server.BrandsResource
import com.allow.brandapp.data.server.ServerDataMapper
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.model.BrandsInfo
import com.android.volley.Response
import timber.log.Timber


object ServiceVolley {

    private val REQUEST_TAG: String = ServiceVolley::class.java.simpleName
    private const val BASE_PATH = "https://api.myjson.com"

    fun getBrandsInfo(path: String, dataMapper: ServerDataMapper = ServerDataMapper()): LiveData<Outcome<BrandsInfo>> {
        val result = MutableLiveData<Outcome<BrandsInfo>>()
        result.value = Outcome.loading(true)
        val accessTokenReq = ServiceRequest(
            url = "$BASE_PATH$path",
            clazz = BrandsResource::class.java,
            listener = Response.Listener { response ->
                Timber.d("Get Brands api call OK! Response: $response")
                result.value = Outcome.success(dataMapper.convertToDomain(response))
            },
            errorListener = Response.ErrorListener { error ->
                Timber.e("Get Brands api call fail! Error: ${error.message}")
                result.value = Outcome.failure(error)
            }
        )
        BrandsInfoApp.instance.addToRequestQueue(
            accessTokenReq,
            REQUEST_TAG
        )
        return result
    }
}
