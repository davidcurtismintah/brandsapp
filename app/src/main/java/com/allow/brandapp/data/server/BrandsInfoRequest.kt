package com.allow.brandapp.data.server

import androidx.lifecycle.LiveData
import com.allow.brandapp.data.server.service.ServiceVolley
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.model.BrandsInfo

class BrandsInfoRequest {

    companion object {
        private const val BRANDS_INFO_PATH = "/bins/1eenaa"
    }

    fun execute(): LiveData<Outcome<BrandsInfo>> {
        return ServiceVolley.getBrandsInfo(BRANDS_INFO_PATH)
    }

}

