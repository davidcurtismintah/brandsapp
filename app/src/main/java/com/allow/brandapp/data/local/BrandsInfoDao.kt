package com.allow.brandapp.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable

@Dao
interface BrandsInfoDao {
    @Query("SELECT * FROM brands_info")
    fun queryBrandsInfo(): Flowable<List<BrandsInfoResource>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBrandsInfo(brandsInfo: BrandsInfoResource)
}