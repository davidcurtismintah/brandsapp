package com.allow.brandapp.data.server

import androidx.lifecycle.LiveData
import com.allow.brandapp.domain.datasource.BrandsInfoDataSource
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.datasource.Provider
import com.allow.brandapp.domain.model.BrandsInfo

class BrandsInfoServer : BrandsInfoDataSource {

    companion object : Provider<BrandsInfoDataSource>() {
        override fun creator(): BrandsInfoDataSource = BrandsInfoServer()
    }

    override fun getBrandsInfo(): LiveData<Outcome<BrandsInfo>> {
        return BrandsInfoRequest().execute()
    }
}
