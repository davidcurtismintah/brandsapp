package com.allow.brandapp.data.server

import com.google.gson.annotations.SerializedName

data class Analysis(
    @SerializedName("id")
    val id: String,
    @SerializedName("phrases")
    val phrases: Phrases,
    @SerializedName("sub_title")
    val subTitle: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("title")
    val title: String
)

data class BrandsResource(
    @SerializedName("analysis")
    val analysis: Analysis,
    @SerializedName("companies")
    val companies: List<Company>,
    @SerializedName("reach")
    val reach: Reach
)

data class Company(
    @SerializedName("color")
    val color: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)

data class Data(
    @SerializedName("change")
    val change: Double,
    @SerializedName("company_id")
    val companyId: String,
    @SerializedName("value")
    val value: Double
)

data class Phrases(
    @SerializedName("delta")
    val delta: String,
    @SerializedName("performance")
    val performance: String,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("winner")
    val winner: String
)

data class Reach(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("id")
    val id: String,
    @SerializedName("sub_title")
    val subTitle: String,
    @SerializedName("title")
    val title: String
)