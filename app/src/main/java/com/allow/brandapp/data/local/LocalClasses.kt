package com.allow.brandapp.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "brands_info")
data class BrandsInfoResource(
    @PrimaryKey val id: String
)
