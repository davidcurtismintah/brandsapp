package com.allow.brandapp.data.server

import com.allow.brandapp.domain.model.Analysis
import com.allow.brandapp.domain.model.Brand
import com.allow.brandapp.domain.model.BrandsInfo
import com.allow.brandapp.domain.model.Reach

class ServerDataMapper {

    fun convertToDomain(brandsResource: BrandsResource): BrandsInfo {
        return BrandsInfo(
            analysis = Analysis(
                id = brandsResource.analysis.id,
                title = brandsResource.analysis.title,
                subTitle = brandsResource.analysis.subTitle,
                text = brandsResource.analysis.text,
                deltaPhrase = brandsResource.analysis.phrases.delta,
                performancePhrase = brandsResource.analysis.phrases.performance,
                summaryPhrase = brandsResource.analysis.phrases.summary,
                winnerPhrase = brandsResource.analysis.phrases.winner
            ),
            reach = Reach(
                id = brandsResource.reach.id,
                title = brandsResource.reach.title,
                subTitle = brandsResource.reach.subTitle,
                companies = brandsResource.reach.data.map {data ->
                    Brand(
                        id = data.companyId,
                        name = brandsResource.companies.first { it.id == data.companyId }.name,
                        color = brandsResource.companies.first { it.id == data.companyId }.color,
                        value = data.value,
                        change = data.change
                    )
                }
            )
        )

    }

}