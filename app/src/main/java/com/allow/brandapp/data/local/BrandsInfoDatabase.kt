package com.allow.brandapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [BrandsInfoResource::class], version = 1)
abstract class BrandsInfoDatabase : RoomDatabase() {
    abstract fun brandsInfoDao(): BrandsInfoDao
}