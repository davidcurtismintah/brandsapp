package com.allow.brandapp.domain.model

class BrandsInfo(
    val analysis: Analysis,
    val reach: Reach
)

class Analysis(
    val id: String,
    val title: String,
    val subTitle: String,
    val text: String,
    val deltaPhrase: String,
    val performancePhrase: String,
    val summaryPhrase: String,
    val winnerPhrase: String
)

class Reach(
    val id: String,
    val title: String,
    val subTitle: String,
    val companies: List<Brand>
)

class Brand(
    val id: String,
    val name: String,
    val color: String,
    val value: Double,
    val change: Double
)
