package com.allow.brandapp.domain.commands

interface Command<out T> {
    fun execute(): T
}