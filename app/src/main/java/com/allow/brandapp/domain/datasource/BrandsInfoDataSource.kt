package com.allow.brandapp.domain.datasource

import androidx.lifecycle.LiveData
import com.allow.brandapp.domain.model.BrandsInfo

interface BrandsInfoDataSource {

    fun getBrandsInfo(): LiveData<Outcome<BrandsInfo>>
}
