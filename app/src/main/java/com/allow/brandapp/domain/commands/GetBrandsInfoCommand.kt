package com.allow.brandapp.domain.commands

import androidx.lifecycle.LiveData
import com.allow.brandapp.domain.datasource.BrandsInfoProvider
import com.allow.brandapp.domain.datasource.Outcome
import com.allow.brandapp.domain.model.BrandsInfo

class GetBrandsInfoCommand(private val brandsInfoProvider: BrandsInfoProvider = BrandsInfoProvider()) :
    Command<LiveData<Outcome<BrandsInfo>>> {

    override fun execute(): LiveData<Outcome<BrandsInfo>> {
        return brandsInfoProvider.getBrandsInfo()
    }

}

