package com.allow.brandapp.domain.datasource

import androidx.lifecycle.LiveData
import com.allow.brandapp.data.server.BrandsInfoServer
import com.allow.brandapp.domain.model.BrandsInfo
import com.allow.brandapp.extensions.firstResult

class BrandsInfoProvider(private val sources: List<BrandsInfoDataSource> = SOURCES) {

    companion object {
        val SOURCES = listOf(BrandsInfoServer.get())
    }

    fun getBrandsInfo(): LiveData<Outcome<BrandsInfo>> = requestToSources {
        it.getBrandsInfo()
    }

    private fun <T : Any> requestToSources(f: (BrandsInfoDataSource) -> T?): T
            = sources.firstResult { f(it) }
}